from uuid import uuid4

from django.contrib.auth.models import User
from django.db.models import (
    Model,
    CharField,
    ForeignKey,
    BooleanField,
    DateTimeField,
    ImageField,
    DecimalField,
    TextField,
    SmallIntegerField,
    SET_NULL,
    PositiveIntegerField,
    ManyToManyField,
    UUIDField,
)

from catalogue.choices import Devices_types


# TODO update
class Item(Model):
    item_name = CharField(max_length=200, null=False, blank=False)
    price = DecimalField(max_digits=7, decimal_places=2, blank=True)

    image = ImageField(null=True, blank=True)
    manufacturer = CharField(max_length=64, null=False, blank=False)
    count_in_stock = PositiveIntegerField(default=0)
    category = SmallIntegerField(choices=Devices_types, null=True, blank=True)
    description_catalogue = TextField(max_length=50, null=True, blank=True)
    description = TextField(null=True, blank=True)
    item_created = DateTimeField(auto_now_add=True)

    @property
    def item_description(self):
        return (
            f"{self.item_name} {self.image}, {self.manufacturer}, "
            f"{self.count_in_stock}, {self.description}"
        )

    def __str__(self):
        return f"{self.item_name}"


class Order(Model):
    user = ForeignKey(User, on_delete=SET_NULL, null=True)
    products = ManyToManyField(Item, blank=True)
    ordered_date = DateTimeField(auto_now_add=True)
    ordered = BooleanField(default=False)
    shipping_cost = DecimalField(max_digits=5, decimal_places=2, blank=True)

    def __str__(self):
        return str(self.ordered_date)

    def get_total_price(self):
        total = 0
        for order_item in self.products.all():
            total += order_item.get_final_price()
        return total


class OrderItem(Model):
    item = ForeignKey(Item, on_delete=SET_NULL, null=True)
    order = ForeignKey(Order, on_delete=SET_NULL, null=True)
    quantity = PositiveIntegerField(null=True, blank=True, default=0)
    price = DecimalField(max_digits=5, decimal_places=2, blank=True)

    def get_total_item_price(self):
        return self.quantity * self.item.price
