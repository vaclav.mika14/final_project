from catalogue.enums import TypeChoice
from catalogue.enums import UnitChoice


Devices_types = (
    (TypeChoice.TV.value, TypeChoice.TV.name),
    (TypeChoice.PC.value, TypeChoice.PC.name),
    (TypeChoice.Notebook.value, TypeChoice.Notebook.name),
    (TypeChoice.Mobile_phone.value, TypeChoice.Mobile_phone.name),
)

Unit_types = (
    (UnitChoice.CZK.value, UnitChoice.CZK.name),
    (UnitChoice.DOL.value, UnitChoice.DOL.name),
    (UnitChoice.EUR.value, UnitChoice.EUR.name),
)
