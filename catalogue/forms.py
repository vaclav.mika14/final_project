from django.forms import (
    ModelForm,
    CharField,
    DecimalField,
    ChoiceField,
    IntegerField,
    DateTimeField,
    SlugField,
)

from catalogue.choices import Devices_types, Unit_types
from catalogue.models import Item, OrderItem


# TODO update
class ItemForm(ModelForm):
    class Meta:
        model = Item
        fields = "__all__"

    item_name = CharField(min_length=2, max_length=64, required=True)
    price = DecimalField(max_digits=7, decimal_places=2, required=True)
    # unit = ChoiceField(choices=Unit_types, required=True)
    category = ChoiceField(choices=Devices_types, required=True)
    count_in_stock = IntegerField(required=True)
    # slug = SlugField(required=True)


def add_to_cart(request, **kwargs):
    item = Item.objects.filter(id=kwargs.get("item_id", "")).first()
    order_item, status = OrderItem.objects.get_or_create(item=item)
