# Generated by Django 3.2.6 on 2021-08-21 09:01

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="Item",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("item_name", models.CharField(max_length=200)),
                (
                    "price",
                    models.DecimalField(blank=True, decimal_places=2, max_digits=7),
                ),
                ("image", models.ImageField(blank=True, null=True, upload_to="")),
                ("manufacturer", models.CharField(max_length=64)),
                ("count_in_stock", models.PositiveIntegerField(default=0)),
                (
                    "category",
                    models.SmallIntegerField(
                        blank=True,
                        choices=[
                            (10, "TV"),
                            (20, "PC"),
                            (30, "Notebook"),
                            (40, "Mobile_phone"),
                        ],
                        null=True,
                    ),
                ),
                (
                    "description_catalogue",
                    models.TextField(blank=True, max_length=50, null=True),
                ),
                ("description", models.TextField(blank=True, null=True)),
                ("item_created", models.DateTimeField(auto_now_add=True)),
                ("slug", models.SlugField()),
            ],
        ),
        migrations.CreateModel(
            name="Order",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("ordered_date", models.DateTimeField(auto_now_add=True)),
                ("ordered", models.BooleanField(default=False)),
                (
                    "shipping_cost",
                    models.DecimalField(blank=True, decimal_places=2, max_digits=5),
                ),
                (
                    "user",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="OrderItem",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "quantity",
                    models.PositiveIntegerField(blank=True, default=0, null=True),
                ),
                (
                    "price",
                    models.DecimalField(blank=True, decimal_places=2, max_digits=5),
                ),
                (
                    "item",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="catalogue.item",
                    ),
                ),
                (
                    "order",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="catalogue.order",
                    ),
                ),
            ],
        ),
    ]
