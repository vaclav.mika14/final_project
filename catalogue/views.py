import json

from django.contrib.auth import authenticate

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import CreateView, TemplateView, DetailView

from .forms import ItemForm
from .models import Item


# Create your views here.
def item_list(request):
    context = {"items": Item.objects.all()}
    return render(request, "item_list.html", context)


class IndexView(TemplateView):
    template_name = "base.html"


class LoginUserView(View):
    def post(self, request):
        # --- Output fields
        output = {"message": ""}

        msg_success = "You are logged in"
        msg_fail = "I do not know you!!!"

        # --- Read client's data
        request_json = json.loads(request.body)

        # --- Required fields
        username = request_json.get("username", "").lower().strip()
        password = request_json.get("password")

        if username and password:
            # --- Authentication
            if authenticate(username=username, password=password):
                output["message"] = msg_success
            else:
                output["message"] = msg_fail
        else:
            output["message"] = msg_fail

        # --- Answer
        return JsonResponse(output)


# TODO update
class ItemCreateView(LoginRequiredMixin, CreateView):
    template_name = "item_list.html"
    form_class = ItemForm
    success_url = reverse_lazy("catalogue:item_list")


class AboutView(TemplateView):
    template_name = "about.html"


class ItemDetailView(DetailView):
    model = Item
    template_name = "product_detail.html"
    queryset = Item.objects.all()
