from enum import IntEnum


class TypeChoice(IntEnum):
    TV = 10
    PC = 20
    Notebook = 30
    Mobile_phone = 40


class UnitChoice(IntEnum):
    CZK = 100
    DOL = 200
    EUR = 300
