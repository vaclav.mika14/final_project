from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import (
    DetailView,
    ListView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.core.exceptions import ObjectDoesNotExist
from catalogue.models import Item
from .models import Cart, CartItem


def cart_id(request):
    cart = request.session.session_key
    if not cart:
        cart = request.session.create()
    return cart


def add_cart(request, item_id):
    product = Item.objects.get(id=item_id)
    cart, _ = Cart.objects.get_or_create(cart_id=cart_id(request), user=request.user)
    cart_item, created = CartItem.objects.get_or_create(product=product, cart=cart)

    if not created:
        cart_item.quantity += 1
        cart_item.save()

    return redirect("cart:cart_detail")


def cart_detail(request, total=0, counter=0, cart_items=None):
    try:
        cart = Cart.objects.get(cart_id=cart_id(request))
        cart_items = CartItem.objects.filter(cart=cart, active=True)
        for cart_item in cart_items:
            total += cart_item.product.price * cart_item.quantity
            counter += cart_item.quantity
    except ObjectDoesNotExist:
        pass
    return render(
        request, "cart.html", dict(cart_items=cart_items, total=total, counter=counter)
    )


def cart_remove(request, product_id):
    cart = Cart.objects.get(cart_id=cart_id(request))
    product = get_object_or_404(Item, id=product_id)
    cart_item = CartItem.objects.get(product=product, cart=cart)
    if cart_item.quantity > 1:
        cart_item.quantity -= 1
        cart_item.save()
    else:
        cart_item.delete()
    return redirect("cart:cart_detail")


def full_remove(request, product_id):
    cart = Cart.objects.get(cart_id=cart_id(request))
    product = get_object_or_404(Item, id=product_id)
    cart_item = CartItem.objects.get(product=product, cart=cart)
    cart_item.delete()
    return redirect("cart:cart_detail")
