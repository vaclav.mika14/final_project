from django.contrib.auth.models import User
from django.db.models import (
    Model,
    ForeignKey,
    CASCADE,
    DateTimeField,
    IntegerField,
    FloatField,
    CharField,
    BooleanField,
)
from django.utils.datetime_safe import datetime

from catalogue.models import Item


class Cart(Model):
    user = ForeignKey(User, on_delete=CASCADE)
    cart_id = CharField(max_length=250, blank=True)
    created_at = DateTimeField(default=datetime.now)

    def __str__(self):
        return self.cart_id


class CartItem(Model):
    product = ForeignKey(Item, on_delete=CASCADE)
    quantity = IntegerField(default=1)
    cart = ForeignKey("Cart", on_delete=CASCADE)
    active = BooleanField(default=True)

    def sub_total(self):
        return self.product.price * self.quantity

    def __str__(self):
        return self.product
