from django.contrib.auth import views
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

from carts.views import add_cart, cart_detail, cart_remove, full_remove

app_name = "cart"

urlpatterns = [
    path("add/<int:item_id>/", add_cart, name="add_cart"),
    path("", cart_detail, name="cart_detail"),
    path("remove/<int:product_id>/", cart_remove, name="cart_remove"),
    path("full_remove/<int:product_id>/", full_remove, name="full_remove"),
]
