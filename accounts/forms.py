from django.contrib.auth.forms import UserCreationForm

from django.db.transaction import atomic
from django.forms import CharField

from accounts.models import Profile


class SignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        fields = [
            "username",
        ]

    personal_phone = CharField(max_length=32, required=False)
    street = CharField(max_length=128, required=False)
    house_number = CharField(max_length=16, required=False)
    city = CharField(max_length=64, required=False)
    zip_code = CharField(max_length=16, required=False)
    country_iso3 = CharField(max_length=3, required=False)

    @atomic
    def save(self, commit=True):
        # INSERT INTO auth_user ...
        result = super().save(commit)

        profile = Profile(
            user=result,
            personal_phone=self.cleaned_data["personal_phone"],
            address=self.cleaned_data["personal_phone"],
        )

        if commit:
            # INSERT INTO accounts_profile ...
            profile.save()

        return result
