from django.contrib.auth.models import User
from django.db.models import (
    CASCADE,
    CharField,
    Model,
    OneToOneField,
    ForeignKey,
    SET_NULL,
    EmailField,
)


class Address(Model):
    street = CharField(max_length=128, null=False, blank=False)
    house_number = CharField(max_length=16, null=False, blank=False)
    city = CharField(max_length=64, null=False, blank=False)
    zip_code = CharField(max_length=16, null=False, blank=False)
    country_iso3 = CharField(max_length=3, null=False, blank=False)

    @property
    def full_address(self):
        return (
            f"{self.street} {self.house_number}, {self.city}, "
            f"{self.country_iso3}, {self.zip_code}"
        )

    def __str__(self):
        return f"{self.full_address} ({self.id})"


class Profile(Model):
    user = OneToOneField(User, on_delete=CASCADE)
    personal_phone = CharField(max_length=32, blank=True, null=True)
    email = EmailField(max_length=60, blank=True, null=True)
    address = ForeignKey(Address, on_delete=SET_NULL, null=True)
