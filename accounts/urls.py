from django.contrib.auth import views
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

from accounts.views import SignUpView, SubmittableLogoutView, view_profile

app_name = "accounts"
urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("logout/request/", SubmittableLogoutView.as_view(), name="logout_request"),
    path("sign_up/", SignUpView.as_view(), name="sign_up"),
    path("profile/", view_profile, name="view_profile"),
]
