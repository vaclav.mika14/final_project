from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView
from accounts.forms import SignUpForm


class SubmittableLogoutView(TemplateView):
    template_name = "registration/logged_out.html"


class SignUpView(CreateView):
    template_name = "form.html"
    form_class = SignUpForm
    success_url = reverse_lazy("index")


def view_profile(request):
    profile = {"profile": request.user}
    return render(request, "account_card.html", profile)
